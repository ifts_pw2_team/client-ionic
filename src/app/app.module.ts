import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { ProdottiElencoPage } from '../pages/prodotti-elenco/prodotti-elenco';
import { ProdottiProvider } from '../providers/prodotti/prodotti';
import { CarrelloPage } from '../pages/carrello/carrello';
import { SpedizionePage } from '../pages/spedizione/spedizione';
import { RiepilogoPage } from '../pages/riepilogo/riepilogo';

@NgModule({
  declarations: [
    MyApp,
    ProdottiElencoPage,
    CarrelloPage,
    SpedizionePage,
    RiepilogoPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProdottiElencoPage,
    CarrelloPage,
    SpedizionePage,
    RiepilogoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ProdottiProvider
  ]
})
export class AppModule {}
