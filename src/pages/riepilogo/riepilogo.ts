import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-riepilogo',
  templateUrl: 'riepilogo.html',
})
export class RiepilogoPage {
  prodotti: any[];
  datiSpedizione: object = {};
  totaleParziale: number = 0;
  costoSpedizione: number = 2;
  url: string = 'http://localhost:8080/ordini';
  error: any = null;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiepilogoPage');
    this.prodotti = this.navParams.get('prodotti');
    this.datiSpedizione = this.navParams.get('datiSpedizione');
    console.log(this.datiSpedizione);
    this.determinaTotalParziale();
  }

  determinaTotalParziale(){
      for (let i = 0; i < this.prodotti.length; i++) {
          this.totaleParziale += this.prodotti[i].prezzo * this.prodotti[i].quantita;
      }
  }

  inviaOrdine() {
    let loader = this.presentLoading();
    let data = this.datiSpedizione;
    data["prodotti"] = this.prodotti;

    this.http.post(this.url, data)
    .subscribe(
      (response) => {
        let res = response.json();
        console.log('risposta ricevuta');
        console.log(res);
        this.dismissLoading(loader);
        this.presentAlert("Ordine confermato.");
        this.navCtrl.popToRoot()
      },
      (error) => {
        this.error = error;
        console.error('errore durante chiamata http!!!');
        console.error(error);
        this.dismissLoading(loader);
        this.presentAlert("Riprovare piu' tardi.", false);
        // this.navCtrl.pop();
      }
    );
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Caricamento...",
      // duration: 3000
    });
    loader.present();
    return loader;
  }

  dismissLoading(loader){
    loader.dismiss();
  }

  presentAlert(msg, success = true) {
    if (success) {
      var title = 'Successo!';
    }
    else{
      var title = 'Errore!';
    }
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }
}
