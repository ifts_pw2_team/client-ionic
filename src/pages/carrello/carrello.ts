import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { SpedizionePage } from './../spedizione/spedizione';

@Component({
  selector: 'page-carrello',
  templateUrl: 'carrello.html',
})
export class CarrelloPage {
  prodotti: any[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CarrelloPage');
    this.prodotti = this.navParams.get('carrello');
  }

  onCheckout(){
    console.log(this.prodotti);
    this.navCtrl.push(SpedizionePage, {prodotti: this.prodotti});
  }
  onAggiornaQuantita(prodotto, evento){
      console.log(prodotto);
      console.log(evento);
      let nuovaQuantita = parseInt((<HTMLInputElement>evento.target).value);
      if (isNaN(nuovaQuantita) || nuovaQuantita < 1) {
          console.error("quantita non valida");
      }
      else{
        console.log("quantita valida");
        this.onAggiornaQuantitaProdotto(prodotto.id, nuovaQuantita);
      }
  }
  onAggiornaQuantitaProdotto(id, qta){
    // console.log(this.prodotti);
    for (let i = 0; i < this.prodotti.length; i++) {
        if (this.prodotti[i].id == id) {
            this.prodotti[i].quantita = qta;
            return true;
        }
    }
    return -1;
  }

}
