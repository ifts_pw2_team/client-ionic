import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ProdottiProvider } from './../../providers/prodotti/prodotti';
import { CarrelloPage } from './../carrello/carrello';

@Component({
  selector: 'page-prodotti-elenco',
  templateUrl: 'prodotti-elenco.html',
})
export class ProdottiElencoPage {
  prodotti: object[] = [];
  error: any = null;
  carrello: any[] = [];
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public prodottiProvider: ProdottiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProdottiElencoPage');
    this.carrello = [];
    this.caricaProdotti();
  }

  caricaProdotti() {
    this.prodottiProvider.getProdotti()
    .subscribe(
      (response) => {
        var arr = response.json();
        var arr2 = [];
        for (let i = 0; i < arr.length; i++) {
            var p = { id: arr[i]["pr_ID"],
              nome: arr[i]["pr_nome"],
              descrizione: arr[i]["pr_descrizione"],
              prezzo: parseFloat(arr[i]["pr_prezzo"]),
              quantita: 0
            };
            console.info(p);
            arr2.push(p);
        }
        this.prodotti = arr2;
        // console.info(response.json());
        // console.info(this.prodotti);
        // console.log('dati ricevuti');
        // console.log(this.dati);
        // this.dismissLoading(loader);
      },
      (error) => {
        this.error = error;
        console.error('errore durante chiamata http!!!');
        console.error(error);
        // this.dismissLoading(loader);
        // if(error.status == 404){
        //   this.presentAlert("Città non trovata.");
        // }
        // else{
          // this.presentAlert(error.statusText);
        // }
        // this.navCtrl.pop();
      }
    );
  }
  prodottoInCarrello(prodotto){
    // console.log(this.carrello);
    for (let i = 0; i < this.carrello.length; i++) {
        if (this.carrello[i] && this.carrello[i].id == prodotto.id) {
            return i;
        }
    }
    return -1;
  }
  onAggiungiRimuoviCarrello(prodotto) {
    // console.log(this.carrello);
    var inCarrelloRes = this.prodottoInCarrello(prodotto);
    // console.log(inCarrelloRes);
    if (inCarrelloRes === -1) {
      prodotto.quantita = 1;
      this.carrello.push(prodotto);
      console.log("pushed");
    }
    else{
      let c = [];
      for (let i = 0; i < this.carrello.length; i++) {
          if (i !== inCarrelloRes) {
            c.push(this.carrello[i]);
          }
      }
      this.carrello = c;
      console.log("deleted");
    }
    // console.log(this.carrello);
  }

  onVisualizzaCarrello(){
    if (this.carrello.length) {
      this.navCtrl.push(CarrelloPage, {carrello: this.carrello});
    }
  }

}
