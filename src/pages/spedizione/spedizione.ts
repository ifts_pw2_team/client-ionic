import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


import { RiepilogoPage } from './../riepilogo/riepilogo';

@Component({
  selector: 'page-spedizione',
  templateUrl: 'spedizione.html',
})
export class SpedizionePage {
  prodotti: object[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpedizionePage');
    this.prodotti = this.navParams.get('prodotti');
    console.log(this.prodotti);
  }

  onSubmitForm(f){
    console.log(f);
    let datiSpedizione = f.form.value;
    this.navCtrl.push(RiepilogoPage, {prodotti: this.prodotti, datiSpedizione: datiSpedizione});
  }

}
