import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';


@Injectable()
export class ProdottiProvider {
  url: string = 'http://localhost:8080/prodotti';
  prodotti: object[] = [];

  constructor(public http: Http) {
  }

  getProdotti(): Observable<Response>{
    return this.http.get(this.url);
  }
}
